package nl.bioinf.meggink.WIS.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class User {
    private String name;
    private String email;
    private String password;
    private Role role;

    public User(){
    }

    public User(String name, String email, Role role){
        this.name = name;
        this.email = email;
        this.role = role;
    }

    public User(String name, String email, String password, Role role){
        this.name = name;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public void setPassword(String password){ this.password = password;}

    public void setRole(Role role){this.role = role;}

    public String getName(){return name;}

    public String getEmail(){return email;}

    public String getPassword(){return password;}

    public Role getRole(){return role;}

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(name, user.name) &&
                Objects.equals(email, user.email);
    }

    @Override
    public int hashCode(){return Objects.hash(name, email);}

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role +
                '}';
    }

    public static List<User> getSome(){
        List<User> users = new ArrayList<>();
        User user;
        user = new User("Henk", "henk@example.com", Role.ADMIN);
        users.add(user);
        user = new User("Roger", "roger@example.com", Role.USER);
        users.add(user);
        user = new User("Diana", "diana@example.com", Role.GUEST);
        users.add(user);

        return users;
    }
}
