package nl.bioinf.meggink.WIS.model;

public enum Role {
    GUEST,
    USER,
    ADMIN;
}
