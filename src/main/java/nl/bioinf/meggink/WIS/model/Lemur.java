package nl.bioinf.meggink.WIS.model;

public class Lemur {
    private String scientificName;
    private String englishName;
    private String dutchName;

    public Lemur(String scientificName, String englishName, String dutchName){
        this.scientificName = scientificName;
        this.englishName = englishName;
        this.dutchName = dutchName;
    }

    public String getSpecies(){
        return this.scientificName;
    }

    public String getEnglishName(){
        return this.englishName;
    }

    public String getDutchName(){
        return this.dutchName;
    }

    @Override
    public String toString(){
        return "Lemur{"+
                "scientific name=" + scientificName +
                ", english name=" + englishName +
                ", dutch name=" + dutchName + '}';
    }
}
