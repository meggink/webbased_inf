package nl.bioinf.meggink.WIS.model;

import java.util.Random;

public class PhraseFactory {
    public static int MAX_PHRASE_COUNT = 4;
    public static String getPhrase(String phraseType) {
        Random random = new Random();
        int phraseIndex = random.nextInt(MAX_PHRASE_COUNT) + 1;
        return Integer.toString(phraseIndex);
    }
}
