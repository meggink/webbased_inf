package nl.bioinf.meggink.WIS.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CollectionClass {
    public static List<Lemur> parseLemur(String filename){
        Path path = Paths.get(filename);

        List<Lemur> lemurs = new ArrayList<>();

        Lemur specie = null;

        try (BufferedReader bufferedReader = Files.newBufferedReader(path)){
            String line;
            int lineNumber = 0;

            while ((line = bufferedReader.readLine()) != null){
                lineNumber ++;
                if (lineNumber == 1) continue;

                String[] elements = line.split(",");
                String scientificName = elements[0];
                String englishName = elements[1];
                String dutchName = elements[2];


                Lemur lemur = new Lemur(scientificName, englishName, dutchName);
                lemurs.add(lemur);
                System.out.println(lemur);

            }

        } catch (IOException ex){
            ex.printStackTrace();
        }
        return lemurs;
    }
}
