package nl.bioinf.meggink.WIS.servlets;

import nl.bioinf.meggink.WIS.config.WebConfig;
import nl.bioinf.meggink.WIS.model.CollectionClass;
import nl.bioinf.meggink.WIS.model.Lemur;
import nl.bioinf.meggink.WIS.model.RequestHistory;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "SpeciesBrowserServlet", urlPatterns = "/home")
public class SpeciesBrowserServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String filename = getInitParameter("fileName");
        System.out.println(filename);

        List<Lemur> lemurs = CollectionClass.parseLemur(filename);

        HttpSession session = request.getSession();

        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(request, response, request.getServletContext(), request.getLocale());
        ctx.setVariable("lemurs", lemurs);
        WebConfig.createTemplateEngine(getServletContext()).process("species-listing", ctx, response.getWriter());
    }
}
