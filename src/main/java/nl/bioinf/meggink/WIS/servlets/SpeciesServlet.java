package nl.bioinf.meggink.WIS.servlets;

import nl.bioinf.meggink.WIS.config.WebConfig;
import nl.bioinf.meggink.WIS.model.CollectionClass;
import nl.bioinf.meggink.WIS.model.Lemur;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name="SpeciesServlet", urlPatterns = "/species.detail")
public class SpeciesServlet extends HttpServlet{
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String filename = "/Users/Marijke/Studie/Thema10/Webbased/webbased_inf/data/SpeciesData.csv";
        String species = request.getParameter("species");
        List<Lemur> lemurList = CollectionClass.parseLemur(filename);

        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(request, response, request.getServletContext(), request.getLocale());

        for (Lemur lemur: lemurList){
            String name = lemur.getSpecies();
            if (name.equals(species)){
                ctx.setVariable("specie", lemur);
            }
        }
        WebConfig.createTemplateEngine(getServletContext()).process("species", ctx, response.getWriter());
    }
}
